/**
 * @file       AudioDevice.hpp
 * @brief      Audio device wrapper.
 *
 * AudioDevice.hpp contains AudioDevice class (derived from AudioStream class)
 * which provides the wrapper for native audio devices.
 *
 * @author     murueka
 * @copyright  Part of MALICE.W, 2018 murueka. All rights reserved.
 */

#ifndef __AUDIOFILTERMW_AUDIODEVICE_HPP__
#define __AUDIOFILTERMW_AUDIODEVICE_HPP__

#include <AudioStream.hpp>

namespace mw {

enum AudioDeviceError {
  kAudioDeviceNoSuitableDevice = kAudioStreamErrorLength,
  kAudioDeviceUnknownError,

  kAudioDeviceErrorLength,
};

class AudioDevice : protected AudioStream {};
class AudioInputDevice : AudioDevice {};

class AudioOutputDevice : protected AudioDevice {
public:
  AudioOutputDevice()  = default;
  ~AudioOutputDevice() = default;

  void bind(AudioStream &stream) {
    bool hasOutput = false;
    bool hasInput  = false;
    stream.getProperty(kAudioStreamHasOutput, hasOutput);
    stream.getProperty(kAudioStreamHasInput, hasInput);
    if (!hasOutput || hasInput) {
      throw kAudioStreamIncompatibleTypeError;
    }

    m_hasStream = true;
    m_Stream    = stream;
  }

  void callback(int bufferSize, float buffer[]) {
    _do_process(m_Stream, bufferSize, buffer, buffer);
  }

private:
  AudioStream &m_Stream;
  bool         m_hasStream;
};

}; // namespace mw

#endif //__AUDIOFILTERMW_AUDIODEVICE_HPP__