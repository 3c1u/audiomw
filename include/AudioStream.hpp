/**
 * @file       AudioStream.hpp
 * @brief      Audio stream and conjunction.
 *
 * AudioStream.hpp contains AudioStream class and AudioStreamConjunction class
 * which enable you to create audio processing chains. You can make your custom
 * AudioStream class by overriding "process(int, float[], float[]) -> void" (and
 * "setProperty<T>(AudioStreamProperty, &T) -> void" or
 * "getProperty<T>(AudioStreamProperty, &T) -> void" if necessary).
 *
 * @author     murueka
 * @copyright  Part of MALICE.W, 2018 murueka. All rights reserved.
 */

#ifndef __AUDIOFILTERMW_AUDIOSTREAM_HPP__
#define __AUDIOFILTERMW_AUDIOSTREAM_HPP__

namespace mw {

class AudioStream;
class AudioStreamConnector;

using AudioStreamTraits = int;

/**
 * @enum AudioStreamTrait
 * Trait for AudioStream.
 */
enum AudioStreamTrait {
  kAudioStreamOutputTrait     = 0x01,
  kAudioStreamInputTrait      = 0x01 << 1,
  kAudioStreamUnbalancedTrait = 0x01 << 2,
};

/**
 * @enum AudioStreamProperty
 * Property name for AudioStream. Each property has its type.
 */
enum AudioStreamProperty {
  kAudioStreamInputType,
  kAudioStreamOutputType,

  kAudioStreamIsBalanced,

  kAudioStreamHasInput,
  kAudioStreamHasOutput,

  kAudioStreamPropertyLength,
};

/**
 * @enum AudioStreamError
 * Error code for AudioStream.
 */
enum AudioStreamError {
  kAudioStreamNoError = 0,

  kAudioStreamUnbalancedError,
  kAudioStreamIncompatibleTypeError,
  kAudioStreamIsActiveError,

  kAudioStreamErrorLength,
};

/* AudioStreamType */
struct AudioStreamType {
  int samplingRate;
  int channels;
};

bool operator!=(const AudioStreamType &a, const AudioStreamType &b) {
  return (a.samplingRate != b.samplingRate) || (a.channels != b.channels);
}

/**
 * @brief Audio stream.
 *
 * AudioStream class provides a wrapper for signal processing. Signal processing
 * is done by "process(int, float[], float[]) -> void" method, so the raw data
 * (such as buffer) is not touched by another class nor object (thus monadic.)
 * This also supports (runtime) type-check and prevents error.
 */
class AudioStream {
public:
  /**
   * @brief          Construct a new AudioStream object
   *
   * @param traits   AudioStream traits.
   */
  AudioStream(AudioStreamTraits traits = kAudioStreamOutputTrait)
      : m_isActive(false), m_Traits(traits) {
    m_isBalanced = !(traits & kAudioStreamUnbalancedTrait);
    m_hasInput   = !!(traits & kAudioStreamHasInput);
    m_hasOutput  = !!(traits & kAudioStreamHasOutput);
  }

  /**
   * @brief Destroy the AudioStream object
   */
  virtual ~AudioStream() {}

  virtual void setProperty(AudioStreamProperty property, bool const &value) {
    if (m_isActive)
      throw AudioStreamError(kAudioStreamIsActiveError);

    switch (property) {
    case kAudioStreamIsBalanced:
      m_isBalanced = value;
      break;
    case kAudioStreamHasInput:
      m_hasInput = value;
      break;
    case kAudioStreamHasOutput:
      m_hasOutput = value;
      break;
    default:
      throw AudioStreamError(kAudioStreamIncompatibleTypeError);
      break;
    }
  }

  virtual void setProperty(AudioStreamProperty    property,
                           AudioStreamType const &value) {
    if (m_isActive)
      throw AudioStreamError(kAudioStreamIsActiveError);

    switch (property) {
    case kAudioStreamInputType:
      m_InputType = value;
      break;
    case kAudioStreamOutputType:
      m_OutputType = value;
      break;
    default:
      throw AudioStreamError(kAudioStreamIncompatibleTypeError);
      break;
    }
  }

  virtual void getProperty(AudioStreamProperty property, bool &value) const {
    switch (property) {
    case kAudioStreamIsBalanced:
      value = m_isBalanced;
      break;
    case kAudioStreamHasInput:
      value = m_hasInput;
      break;
    case kAudioStreamHasOutput:
      value = m_hasOutput;
      break;
    default:
      throw AudioStreamError(kAudioStreamIncompatibleTypeError);
      break;
    }
  }

  virtual void getProperty(AudioStreamProperty property,
                           AudioStreamType &   value) const {
    switch (property) {
    case kAudioStreamInputType:
      value = m_InputType;
      break;
    case kAudioStreamOutputType:
      value = m_OutputType;
      break;
    default:
      throw AudioStreamError(kAudioStreamIncompatibleTypeError);
      break;
    }
  }

  virtual void activate() {
    checkIntegrity();
    m_OutputChannelCount = m_OutputType.channels;
    m_isActive           = true;
  }

  virtual void deactivate() { m_isActive = false; }

protected:
  /**
   * @brief                Process audio using protected method.
   *
   * Since "process(int, float[], float[]) -> void" is protected method, it may
   * not be called using pointer or reference. This enables you to call the
   * process method from the outside (see "AudioStreamConjunction::process(int,
   * float[], float[]) -> void" for details.)
   *
   * @param stream         AudioStream.
   * @param bufferSize     Size of buffer.
   * @param buffer_input   Buffer for input signal.
   * @param buffer_output  Buffer for uutput signal.
   */
  void _do_process(AudioStream &stream, int bufferSize, float buffer_input[],
                   float buffer_output[]) {
    stream.process(bufferSize, buffer_input, buffer_output);
  }

  /**
   * @brief                Audio processing callback
   *
   * All the signal processing are done in this method.
   * If the buffer is stereo, for example, it goes like "LRLRLRLRLR..."
   *
   * @param bufferSize     Size of buffer.
   * @param buffer_input   Buffer for input signal.
   * @param buffer_output  Buffer for uutput signal.
   */
  virtual void process(int bufferSize, float buffer_input[],
                       float buffer_output[]) {
    if (!m_isActive)
      return;
    if (!m_hasOutput)
      return;

    for (int i = 0; i < bufferSize * m_OutputChannelCount; i++)
      buffer_output[i] = 0.0f;
  }

  /**
   * @brief               Integrity check.
   */
  virtual void checkIntegrity() {
    if (m_isBalanced && m_hasOutput && m_hasInput)
      if (m_InputType != m_OutputType)
        throw AudioStreamError(kAudioStreamUnbalancedError);

    if ((m_InputType.channels == 0) && m_hasInput)
      throw AudioStreamError(kAudioStreamIncompatibleTypeError);

    if ((m_OutputType.channels == 0) && m_hasOutput)
      throw AudioStreamError(kAudioStreamIncompatibleTypeError);
  }

  bool m_isActive;

  bool m_isBalanced;
  bool m_hasOutput;
  bool m_hasInput;

  int m_OutputChannelCount;

  AudioStreamType m_InputType;
  AudioStreamType m_OutputType;

  AudioStreamTraits m_Traits;
};

class AudioStreamConjunction : public AudioStream {
public:
  AudioStreamConjunction(AudioStream &s1, AudioStream &s2)
      : _s1(s1), _s2(s2),
        AudioStream(kAudioStreamInputTrait | kAudioStreamOutputTrait |
                    kAudioStreamUnbalancedTrait) {}

private:
  using super = AudioStream;

  AudioStream &_s1;
  AudioStream &_s2;

  int m_TmpChannelCount;

protected:
  virtual void process(int bufferSize, float buffer_input[],
                       float buffer_output[]) override {
    if (!m_isActive)
      return;

    float bTmp[bufferSize * m_TmpChannelCount];

    _do_process(_s1, bufferSize, buffer_input, bTmp);
    _do_process(_s2, bufferSize, bTmp, buffer_output);
  }

  virtual void checkIntegrity() override {
    // call intergrity check of superclass
    super::checkIntegrity();

    // check input-output format
    AudioStreamType output1;
    AudioStreamType input2;
    bool            b1, b2;

    _s1.getProperty(kAudioStreamOutputType, output1);
    _s2.getProperty(kAudioStreamInputType, input2);

    _s1.getProperty(kAudioStreamHasOutput, b1);
    _s2.getProperty(kAudioStreamHasInput, b2);

    // if the conjunction "_s1 >> _s2" is not valid, throw an error
    if ((b1 && b2) || (output1 != input2))
      throw AudioStreamError(kAudioStreamIncompatibleTypeError);
  }
};

AudioStreamConjunction operator>>(AudioStream &s1, AudioStream &s2) {
  return AudioStreamConjunction(s1, s2);
}

}; // namespace mw

#endif //__AUDIOFILTERMW_AUDIOSTREAM_HPP__
