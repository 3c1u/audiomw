/**
 * @file       AudioDevice_Mac.cpp
 * @brief      Audio device wrapper for macOS.
 *
 * AudioDevice.hpp contains AudioDevice class (derived from AudioStream class)
 * which provides the wrapper for macOS native audio devices.
 *
 * @author     murueka
 * @copyright  Part of MALICE.W, 2018 murueka. All rights reserved.
 */

#include <AudioDevice.hpp>
#include <AudioStream.hpp>

#include <AudioUnit/AudioUnit.h>
#include <CoreServices/CoreServices.h>

#include <stdexcept>

namespace mw {

class _AudioOutputDeviceMac;

class _AudioOutputDeviceMac : public AudioOutputDevice {
public:
  AudioUnit m_Unit;

  static AudioDevice *GetDefaultAudioOutput() {
    _AudioOutputDeviceMac *self = new _AudioOutputDeviceMac();

    AudioUnit                 output;
    AudioComponentDescription cd;
    AudioComponent            cp;
    AURenderCallbackStruct    callback;
    UInt32                    sizePtr;
    OSStatus                  err = noErr;

    cd.componentType         = kAudioUnitType_Output;
    cd.componentSubType      = kAudioUnitSubType_DefaultOutput;
    cd.componentManufacturer = kAudioUnitManufacturer_Apple;
    cd.componentFlags        = 0;
    cd.componentFlagsMask    = 0;

    cp = AudioComponentFindNext(NULL, &cd);
    AudioComponentInstanceNew(cp, (AudioComponentInstance *)&output);

    AudioStreamBasicDescription desc;
    UInt32                      descSize = sizeof(desc);
    err = AudioUnitGetProperty(output, kAudioUnitProperty_StreamFormat,
                               kAudioUnitScope_Output, 0, &desc, &descSize);

    if (err == noErr) {
      AudioStreamType type = {.samplingRate = int(desc.mSampleRate),
                              .channels     = int(desc.mChannelsPerFrame)};
      self->setProperty(kAudioStreamOutputType, type);
    } else
      throw kAudioDeviceNoSuitableDevice;

    callback.inputProc       = audiodevice_output_callback;
    callback.inputProcRefCon = self;

    err = AudioUnitSetProperty(output, kAudioUnitProperty_SetRenderCallback,
                               kAudioUnitScope_Input, 0, &callback,
                               sizeof(callback));
    if (err != noErr)
      throw kAudioDeviceUnknownError;

    AudioUnitInitialize(output);
    AudioOutputUnitStart(output);

    self->m_Unit = output;

    return self;
  }

  ~_AudioOutputDeviceMac() {
    AudioOutputUnitStop(m_Unit);
    this->deactivate();
  }

  _AudioOutputDeviceMac() {}

private:
};

AudioOutputDevice *GetDefaultAudioOutputDevice() {
  return _AudioOutputDeviceMac::GetDefaultAudioOutput();
}

AudioDevice *ReleaseNativeAudioDevice(_AudioOutputDeviceMac *dev) {
  _AudioOutputDeviceMac *d = (_AudioOutputDeviceMac *)dev;
  delete d;
  return nullptr;
}

/* callback for audio device */
OSStatus audiodevice_output_callback(void *                      inRefCon,
                                     AudioUnitRenderActionFlags *ioActionFlags,
                                     const AudioTimeStamp *      inTimeStamp,
                                     UInt32 inBusNumber, UInt32 inNumberFrames,
                                     AudioBufferList *ioData) {
  _AudioOutputDeviceMac *aStream;
  UInt32                 channelCount = ioData->mNumberBuffers;
  float                  buffer[channelCount * inNumberFrames];

  aStream = (_AudioOutputDeviceMac *)inRefCon;

  try {
    aStream->callback(inNumberFrames, buffer);
  } catch (AudioStreamError _) {
    return -1;
  }

  UInt32 frame   = 0;
  UInt32 channel = 0;
  float *fBuf;

  for (frame = 0; frame < inNumberFrames; frame++) {
    for (channel = 0; channel < channelCount; channel++) {
      fBuf        = (float *)ioData->mBuffers[channel].mData;
      fBuf[frame] = buffer[frame * channelCount + channel];
    }
  }

  return noErr;
}

}; // namespace mw
