/**
 * @file       AudioDevice_Win.cpp
 * @brief      Audio device wrapper for Windows.
 *
 * AudioDevice.hpp contains AudioDevice class (derived from AudioStream class)
 * which provides the wrapper for Windows native audio devices.
 *
 * @author     murueka
 * @copyright  Part of MALICE.W, 2018 murueka. All rights reserved.
 */

#include <AudioDevice.hpp>
#include <AudioStream.hpp>

#include <DxLib.h>
#include <dsound.h>

#pragma comment(lib, "dsound.lib")

namespace mw {

class _AudioOutputDeviceWindows;

class _AudioOutputDeviceWindows : public AudioOutputDevice {
public:
  ~_AudioOutputDeviceWindows() {}

  static _AudioOutputDeviceWindows *GetDefaultAudioOutput() {
    HRESULT err;
    HWND    hWnd = GetMainWindowHandle();

    // allocate an _AudioOutputDeviceWindows instance.
    auto self = new _AudioOutputDeviceWindows();

    // create DirectSound8
    err = DirectSoundCreate8(nullptr, &self->m_lpds, nullptr);
    if (FAILED(err))
      throw AudioDeviceError(kAudioDeviceUnknownError);

    err = self->m_lpds->SetCooperativeLevel(hWnd, DSSCL_PRIORITY);
    if (FAILED(err))
      throw AudioDeviceError(kAudioDeviceUnknownError);

    return self;
  }

private:
  _AudioOutputDeviceWindows();

  LPDIRECTSOUND8 m_lpds;
};

AudioOutputDevice *GetDefaultAudioOutputDevice() {
  return _AudioOutputDeviceWindows::GetDefaultAudioOutput();
}

AudioDevice *ReleaseNativeAudioDevice(AudioDevice *dev) {
  _AudioOutputDeviceWindows *d = (_AudioOutputDeviceWindows *)dev;
  delete d;
  return nullptr;
}

}; // namespace mw