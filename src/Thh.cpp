// THHファイル

#include <cmath>
#include <vector>

namespace THH {

class THH;
class THHChannel;
class THHEvent;

float sawf(float x) {
  float _x = x / (M_PI * 2);
  return 2 * (_x - std::floor(_x)) - 1;
}

float trif(float x) {
  float _x = x / (M_PI * 2) - 0.25;
  float _y = std::abs(2 * (_x - std::floor(_x)) - 1);
  return (2 * _y - 1);
}

float sqf(float x, float duty) {
  float _x = x / (M_PI * 2);
  return (_x - std::floor(_x)) < duty ? 1 : -1;
}

struct Timecode {
  int frame;
};

class THH {
public:
  THH();
  void open(const char *filename);

  THHChannel &channelAt(int channelCount);

  void headAt(Timecode time);

private:
  std::vector<THHChannel> m_Channels;
};

class THHChannel {
public:
  THHChannel(int channelNumber);

  void headAt(Timecode time);
  bool nextEvent(THHEvent &event);
  int nextEventUntil(THHEvent &event, Timecode time);

private:
  std::vector<THHEvent> m_Cvent;
};

class THHEvent {
public:
  Timecode position;
  Timecode length;

  int volume;
  double frequency;

  int duty;

  double pitchbendFreq;
  double fadeAmount;
};

}; // namespace THH